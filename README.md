# [Beamformation Toolbox](SourceCode) #

The Beamformation Toolbox was written by [Svetoslav Ivanov Nikolov](https://www.linkedin.com/in/svetoslavnikolov/)
as a tool during the investigations of synthetic aperture focusing around 2001.
Initially, the toolbox was for use only in Matlab. The last supported (compiled)
version is Matlab 2011 for Windows and Mac.

Python support was added in 2014 for both Windows and Mac.

**To download the repository (without git) click on download button on the panel to the right.**

## Contents of the repository ##

* Contains code for delay-and-sum beamforming for Synthetic Transmit Aperture Ultrasound Imaging.
* Version 1.3
* Website [https://svetoslavnikolov.wordpress.com/beamformation-toolbox/](https://svetoslavnikolov.wordpress.com/beamformation-toolbox/)

## Getting started ##

If are using 64-bit version of Matlab 2011 in Windows, then it is straight-forward:

 - Download the directory [SourceCode/matbft/](SourceCode/matbft/) to your computer.
 - Download the directory [SourceCode/bin](SourceCode/bin) to your computer.
 - Copy `bft.mexw64` to the `matbft` directory (the DLL must be in the same directory as the `.m` files)
 - Add `matbft` to the path in Matlab

If you are using a 64-bit version of Python 2.7.x in either Windows or Mac, the process is also sufficiently simple:

 - Download the directory [SourceCode/pybft](SourceCode/pybft) to your computer.
 - Download the directory [SourceCode/bin/python](SourceCode/bin/python) to your computer
 - Copy the relevant DLL from `bin/python` to `pybft`

The Python version is based on [`ctypes`](https://docs.python.org/3/library/ctypes.html), 
the DLL is loaded at runtime and chances are that it will work, independent of the exact 
version of Python. `libbft.dylib` was last compiled for macOS Sierra.

## Compilation ##

### Matlab ###
    
 - Start Matlab
 - Make sure that you have the right version of the C compiler.
 - Change directory to [`SourceCode/matbft`](SourceCode/matbft)
 - Run the function `mex_beamform.m`
 *Please note that you may need to fix some of the source code, as Matlab interface may have changed over time*

### Python ###
 - To compile for Python, you need [CMake](https://cmake.org/).
 - Create a new project using CMake 
 - Build the project

### Who do I talk to? ###

This repository is maintained by [Svetoslav Ivanov Nikolov](https://www.linkedin.com/in/svetoslavnikolov/).
If you produce some results using the toolbox, please add a reference to 

   J A Jensen, S I Nikolov, Fast simulation of ultrasound images Proc. IEEE Ultrason. Symp., p. 1721-1724, 2000

It will be appreciated. You can get the paper from here: 
[https://svetoslavnikolov.wordpress.com/beamformation-toolbox/](https://svetoslavnikolov.wordpress.com/beamformation-toolbox/)
