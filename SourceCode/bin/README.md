Pre-built binaries
==================
The two subdirectories [matlab](matlab) and [python](python) contain pre-built libraries for Matlab and Python.
These may be out of date, as they have been built some time ago.

[Matlab](matlab)
---------

 - `bft.mexglx` and `bft.mexlx` were built in 2003
 - `bft.mexmaci64` was built in 2011 for Mac and Matlab 2011
 - `bft.mexw32` was built in 2008 for 32-bit Matlab in Windows (whatever version that was)
 - `bft.mexw64` was built in 2013 for Matlab 2011 in Windows - 64-bit Matlab


[Python](python)
-------
 - `bft.dll` is a 64-bit Windows DLL. It has been tested with 64-bit Python 2.7.6
    It should be possible to use the DLL from other 64-bit versions of Python, as it is loaded 
    at run-time using [`ctypes`](https://docs.python.org/3/library/ctypes.html).

 - `libbft.dylib` is a 64-bit Mac OS DLL. It has been tested with 64-bit Python 2.7.6 
    on Mac in 2015.
